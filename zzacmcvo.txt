      *                                                                 00010000
      * FIND VALID OPTIONS FOR - ACTION                                 00020000
      *                                                                 00030000
              EXEC SQL DECLARE VO2 CURSOR FOR                           00040000
                   SELECT                                               00050000
                          ACTION, SELTXT                                00060000
                      FROM VOPTVAL                                      00070000
                      WHERE MAJSYS    = :INAREA.MAJSYS                  00080000
                            AND ACTION   <> ' '                         00090000
                            AND OBJFLD    = '  '                        00100000
                      ORDER BY ACTION ASC END-EXEC.                     00110000
      *                                                                 00120000
      * FIND VALID OPTIONS FOR - OBJFLD                                 00130000
      *                                                                 00140000
              EXEC SQL DECLARE VO3 CURSOR FOR                           00150000
                   SELECT                                               00160000
                          OBJFLD, SELTXT                                00170000
                      FROM VOPTVAL                                      00180000
                      WHERE MAJSYS    = :INAREA.MAJSYS                  00190000
                            AND ACTION    = :INAREA.ACTION              00200000
                            AND OBJFLD   <> '  '                        00210000
                            AND SRCHCRIT  = '  '                        00220000
                      ORDER BY OBJFLD ASC END-EXEC.                     00230000
      *                                                                 00240000
      * FIND VALID OPTIONS FOR - SEARCH CRITERION                       00250000
      *                                                                 00260000
              EXEC SQL DECLARE VO4 CURSOR FOR                           00270000
                   SELECT                                               00280000
                          SRCHCRIT, SELTXT                              00290000
                      FROM VOPTVAL                                      00300000
                      WHERE MAJSYS    = :INAREA.MAJSYS                  00310000
                            AND ACTION    = :INAREA.ACTION              00320000
                            AND OBJFLD    = :INAREA.OBJFLD              00330000
                            AND SRCHCRIT <> '  '                        00340000
                            AND (SCRTYPE  = ' ' OR SCRTYPE = 'S')       00350000
                      ORDER BY SRCHCRIT ASC END-EXEC.                   00360000
