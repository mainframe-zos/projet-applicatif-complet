//FORSYSXZ JOB  CLASS=A,MSGCLASS=X,REGION=0M,NOTIFY=&SYSUID
//* ------------------------------------------------------------------
//*  ZZAC : INSTALLATION TWS 950
//* ----------------------------------------------------------
//* ALLOCATION DATA STORE VSAM DATASETS POUR ZZnF
//* Passer ce job une fois pour ZZnF avec SYSTWS=F
//* Puis passer pour ZZnG avec SYSTWS=G
//* ------------------------------------------------------------------
//E1       EXPORT SYMLIST=(SYSTWS)
//SYSTWS   SET    SYSTWS=F
//*SYSTWS   SET    SYSTWS=G
//* ----------------------------------------------------------
//*
//*
//ALLOCATE EXEC PGM=IDCAMS,REGION=512K
//*
//* ALLOCATE   ZWS   DATA STORE VSAM DATASETS
//*
//* NOTE: TO PREVENT DELETION OF VSAM DATASETS BY MISTAKE DATASET
//*       DELETION IS INDICATED AS COMMENTS ONLY.
//* NOTE: IF MIGRATING TO A DIFFERENT RELEASE OF ZWS AND THE       @31A
//*       DATASTORE FILES (EQQPKI01, EQQSKI01, EQQSDFnn,           @31A
//*       EQQUDFnn) EXIST, THIS JOB MAY BE SKIPPED AND             @31A
//*       THE EXISTING DATASTORE FILES MAY BE USED                 @31A
//*
//SYSPRINT DD SYSOUT=*
//SYSIN    DD *,SYMBOLS=JCLONLY
   DELETE ('ZZOSSMS.TWCZ.&SYSTWS..DS.PKI01') +
      CLUSTER
  IF LASTCC = 8 THEN +
    SET MAXCC = 0
    DEFINE CLUSTER(NAME(ZZOSSMS.TWCZ.&SYSTWS..DS.PKI01) +
                   CYLINDERS(10,20)        +
                   VOLUMES(ZZSMS1)       +
                   KEYS(34,0)              +
                   RECORDSIZE(77,77)       +
                   CISZ(4096)              +
                   UNIQUE                  +
                   INDEXED                 +
                   SHR(1,3)                +
                   FREESPACE(10,10)        +
           )
  /* CATALOG() */
   DELETE ('ZZOSSMS.TWCZ.&SYSTWS..DS.SKI01') +
      CLUSTER
  IF LASTCC = 8 THEN +
    SET MAXCC = 0
    DEFINE CLUSTER(NAME(ZZOSSMS.TWCZ.&SYSTWS..DS.SKI01) +
                   CYLINDERS(5,5)          +
                   VOLUMES(ZZSMS1)       +
                   KEYS(38,0)              +
                   RECORDSIZE(76,32000)    +
                   CISZ(4096)              +
                   UNIQUE                  +
                   INDEXED                 +
                   SHR(1,3)                +
                   FREESPACE(10,10)        +
           )
  /* CATALOG() */
   DELETE ('ZZOSSMS.TWCZ.&SYSTWS..DS.SDF01') +
      CLUSTER
  IF LASTCC = 8 THEN +
    SET MAXCC = 0
    DEFINE CLUSTER (NAME(ZZOSSMS.TWCZ.&SYSTWS..DS.SDF01) +
                   VOLUMES(ZZSMS1)       +
                   CYLINDERS(30,50)        +
                   SHAREOPTIONS(2,3)       +
                   LINEAR                  +
           )
  /* CATALOG() */
   DELETE ('ZZOSSMS.TWCZ.&SYSTWS..DS.SDF02') +
      CLUSTER
  IF LASTCC = 8 THEN +
    SET MAXCC = 0
    DEFINE CLUSTER (NAME(ZZOSSMS.TWCZ.&SYSTWS..DS.SDF02) +
                   VOLUMES(ZZSMS1)       +
                   CYLINDERS(30,50)        +
                   SHAREOPTIONS(2,3)       +
                   LINEAR                  +
           )
  /* CATALOG() */
   DELETE ('ZZOSSMS.TWCZ.&SYSTWS..DS.SDF03') +
      CLUSTER
  IF LASTCC = 8 THEN +
    SET MAXCC = 0
    DEFINE CLUSTER (NAME(ZZOSSMS.TWCZ.&SYSTWS..DS.SDF03) +
                   VOLUMES(ZZSMS1)       +
                   CYLINDERS(30,50)        +
                   SHAREOPTIONS(2,3)       +
                   LINEAR                  +
           )
  /* CATALOG() */
   DELETE ('ZZOSSMS.TWCZ.&SYSTWS..DS.UDF01') +
      CLUSTER
  IF LASTCC = 8 THEN +
    SET MAXCC = 0
    DEFINE CLUSTER (NAME(ZZOSSMS.TWCZ.&SYSTWS..DS.UDF01) +
                   VOLUMES(ZZSMS1)       +
                   CYLINDERS(30,50)        +
                   SHAREOPTIONS(2,3)       +
                   LINEAR                  +
           )
  /* CATALOG() */
   DELETE ('ZZOSSMS.TWCZ.&SYSTWS..DS.UDF02') +
      CLUSTER
  IF LASTCC = 8 THEN +
    SET MAXCC = 0
    DEFINE CLUSTER (NAME(ZZOSSMS.TWCZ.&SYSTWS..DS.UDF02) +
                   VOLUMES(ZZSMS1)       +
                   CYLINDERS(30,50)        +
                   SHAREOPTIONS(2,3)       +
                   LINEAR                  +
           )
  /* CATALOG() */
   DELETE ('ZZOSSMS.TWCZ.&SYSTWS..DS.UDF03') +
      CLUSTER
  IF LASTCC = 8 THEN +
    SET MAXCC = 0
    DEFINE CLUSTER (NAME(ZZOSSMS.TWCZ.&SYSTWS..DS.UDF03) +
                   VOLUMES(ZZSMS1)       +
                   CYLINDERS(30,50)        +
                   SHAREOPTIONS(2,3)       +
                   LINEAR                  +
           )
  /* CATALOG() */
//*
//*----------------------------------------------------------------*
//* INITIALIZE PRIMARY INDEX
//*----------------------------------------------------------------*
//*
//INIPKI   EXEC PGM=SORT
//SYSOUT   DD SYSOUT=*
//SORTIN   DD *
        00010101
/*
//SORTOUT  DD DSN=ZZOSSMS.TWCZ.&SYSTWS..DS.PKI01,DISP=SHR
//DFSPARM  DD *
  RECORD TYPE=V
  SORT FIELDS=(1,16,CH,A)
  OUTREC FIELDS=(9:C'00010101',61Z)
//*
//*----------------------------------------------------------------*
//* PREPARE SECONDARY INDEX HEADER RECORD
//*----------------------------------------------------------------*
//INITSKI1 EXEC PGM=SORT
//SYSOUT   DD SYSOUT=*
//SORTIN   DD *
0
/*
//SORTOUT  DD DSN=ZZOSSMS.TWCZ.SKIHDR,
//           DISP=(NEW,CATLG),UNIT=SYSDA,
//           DCB=(RECFM=F,LRECL=76,BLKSIZE=76),SPACE=(TRK,(1))
//DFSPARM  DD *
  RECORD TYPE=F
  SORT FIELDS=(1,1,CH,A)
  OUTREC FIELDS=(76X'00')
//*
//*----------------------------------------------------------------*
//* INITIALIZE SECONDARY INDEX
//*----------------------------------------------------------------*
//INITSKI2 EXEC PGM=IDCAMS
//SYSPRINT DD SYSOUT=*
//SYSIN    DD *,SYMBOLS=JCLONLY
           REPRO INDATASET(ZZOSSMS.TWCZ.SKIHDR)-
           OUTDATASET(ZZOSSMS.TWCZ.&SYSTWS..DS.SKI01)
//*----------------------------------------------------------------*
//* DELETE INIT INPUT FILE
//*----------------------------------------------------------------*
//INITSKI3 EXEC PGM=IEFBR14
//SORTOUT  DD DSN=ZZOSSMS.TWCZ.SKIHDR,
//            DISP=(OLD,DELETE,DELETE)
