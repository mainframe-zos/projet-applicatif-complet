//FORSYSXZ JOB  CLASS=A,MSGCLASS=X,REGION=0M,NOTIFY=&SYSUID
//* ------------------------------------------------------------------
//*  ZZAC : HSM ALLOC DES BACKUP
//* ------------------------------------------------------------------
//ALLOCBK EXEC PGM=IEFBR14
//*
//*****************************************************************/
//*    THIS SAMPLE JOB ALLOCATES AND CATALOGS THE CONTROL DATA SET*/
//*    BACKUP VERSION DATA SETS ON DASD VOLUMES.                  */
//*                                                               */
//*    ENSURE THAT BACKUP VERSION DATA SETS ARE PLACED ON VOLUMES */
//*    THAT ARE DIFFERENT FROM THE VOLUMES THAT THE CONTROL DATA  */
//*    SETS ARE ON.                                               */
//*                                                               */
//*    THIS SAMPLE JOB ALLOCATES FOUR BACKUP COPIES (THE DEFAULT) */
//*    FOR EACH CONTROL DATA SET.  IF YOU SPECIFY A DIFFERENT     */
//*    NUMBER OF BACKUP VERSIONS, ENSURE THAT YOU ALLOCATE A      */
//*    BACKUP COPY FOR EACH OF THE BACKUP VERSIONS YOU SPECIFY.   */
//*****************************************************************/
//*                                                               */
//*    EDIT THIS JCL TO REPLACE THE PARAMETERS DESCRIBED BELOW.   */
//*                                                               */
//*****************************************************************/
//*  PARAMETER PARAMETER DEFINITION
//*
//*  3390       -  UNIT TYPE OF VOLUME TO CONTAIN THE FIRST CDS
//*                BACKUP VERSION.
//*  3390       -  UNIT TYPE OF VOLUME TO CONTAIN THE SECOND CDS
//*                BACKUP VERSION.
//*  3390       -  UNIT TYPE OF VOLUME TO CONTAIN THE THIRD CDS
//*                BACKUP VERSION.
//*  3390       -  UNIT TYPE OF VOLUME TO CONTAIN THE FOURTH CDS
//*                BACKUP VERSION.
//*  ?BKVOL1    -  VOLUME SERIAL OF VOLUME TO CONTAIN THE FIRST CDS
//*                BACKUP VERSION.
//*  ?BKVOL2    -  VOLUME SERIAL OF VOLUME TO CONTAIN THE SECOND CDS
//*                BACKUP VERSION.
//*  ?BKVOL3    -  VOLUME SERIAL OF VOLUME TO CONTAIN THE THIRD CDS
//*                BACKUP VERSION.
//*  ?BKVOL4    -  VOLUME SERIAL OF VOLUME TO CONTAIN THE FOURTH CDS
//*                BACKUP VERSION.
//*  SCHSMOPL   -  STORAGE CLASS NAME FOR BACKUP VERSIONS
//*  MCHSMOPL   -  MANAGEMENT CLASS NAME OF THE HSM CONSTRUCT
//*
//*  ?CDSSIZE   -  NUMBER OF CYLINDERS ALLOCATED TO CDS BACKUP
//*                VERSIONS.
//*  ?JNLSIZE   -  NUMBER OF CYLINDERS ALLOCATED TO JOURNAL DATA
//*                SETS.
//*  ?UID       -  AUTHORIZED USER ID (1 TO 7 CHARS) FOR THE HSM-
//*                STARTED PROCEDURE. THIS WILL BE USED AS THE
//*                HIGH-LEVEL QUALIFIER OF HSM DATA SETS.
//* (NOTE: UID AUTHORIZATION IS VALID IN A NON-FACILITY CLASS    */
//* ENVIRONMENT ONLY, OTHERWISE, FACILITY CLASS PROFILES WILL BE */
//* USED FOR AUTHORIZATION CHECKING.)                            */
//*****************************************************************/
//*
//******************************************************************/
//* REMOVE THE NEXT FOUR DD STATEMENTS IF YOU DO NOT INTEND TO USE */
//* BACKUP AND DUMP                                                */
//*                                                                */
//* THIS PROCEDURE ASSUMES A SINGLE CLUSTER BCDS.  IF MORE THAN    */
//* ONE VOLUME IS DESIRED, FOLLOW THE RULES FOR A MULTICLUSTER     */
//* CDS.                                                           */
//******************************************************************/
//*
//BCDSV1 DD DSN=ZZOSHSM.BCDS.BACKUP.D0000001,DISP=(,CATLG),UNIT=3390,
//   VOL=SER=ZZSMS1,SPACE=(CYL,(20,5))
//BCDSV2 DD DSN=ZZOSHSM.BCDS.BACKUP.D0000002,DISP=(,CATLG),UNIT=3390,
//   VOL=SER=ZZSMS2,SPACE=(CYL,(20,5))
//BCDSV3 DD DSN=ZZOSHSM.BCDS.BACKUP.D0000003,DISP=(,CATLG),UNIT=3390,
//   VOL=SER=ZZSMS3,SPACE=(CYL,(20,5))
//*
//******************************************************************/
//* THIS PROCEDURE ASSUMES A SINGLE CLUSTER MCDS.  IF MORE THAN    */
//* ONE VOLUME IS DESIRED, FOLLOW THE RULES FOR A MULTICLUSTER     */
//* CDS.                                                           */
//******************************************************************/
//*
//MCDSV1 DD DSN=ZZOSHSM.MCDS.BACKUP.D0000001,DISP=(,CATLG),UNIT=3390,
//   VOL=SER=ZZSMS2,SPACE=(CYL,(10,5))
//MCDSV2 DD DSN=ZZOSHSM.MCDS.BACKUP.D0000002,DISP=(,CATLG),UNIT=3390,
//   VOL=SER=ZZSMS3,SPACE=(CYL,(10,5))
//MCDSV3 DD DSN=ZZOSHSM.MCDS.BACKUP.D0000003,DISP=(,CATLG),UNIT=3390,
//   VOL=SER=ZZSMS1,SPACE=(CYL,(10,5))
//*
//******************************************************************/
//* REMOVE THE NEXT FOUR DD STATEMENTS IF YOU DO NOT INTEND TO USE */
//* TAPE VOLUMES FOR DAILY BACKUP VOLUMES, SPILL BACKUP VOLUMES,   */
//* OR MIGRATION LEVEL 2 VOLUMES.                                  */
//*                                                                */
//* THE OCDS MAY NOT EXCEED 1 VOLUME.                              */
//******************************************************************/
//*
//OCDSV1 DD DSN=ZZOSHSM.OCDS.BACKUP.D0000001,DISP=(,CATLG),UNIT=3390,
//   VOL=SER=ZZSMS3,SPACE=(CYL,(5,5))
//OCDSV2 DD DSN=ZZOSHSM.OCDS.BACKUP.D0000002,DISP=(,CATLG),UNIT=3390,
//   VOL=SER=ZZSMS1,SPACE=(CYL,(5,5))
//OCDSV3 DD DSN=ZZOSHSM.OCDS.BACKUP.D0000003,DISP=(,CATLG),UNIT=3390,
//   VOL=SER=ZZSMS2,SPACE=(CYL,(5,5))
//*
//JRNLV1 DD DSN=ZZOSHSM.JRNL.BACKUP.D0000001,DISP=(,CATLG),UNIT=3390,
//   VOL=SER=ZZSMS1,SPACE=(CYL,(5,5))
//JRNLV2 DD DSN=ZZOSHSM.JRNL.BACKUP.D0000002,DISP=(,CATLG),UNIT=3390,
//   VOL=SER=ZZSMS2,SPACE=(CYL,(5,5))
//JRNLV3 DD DSN=ZZOSHSM.JRNL.BACKUP.D0000003,DISP=(,CATLG),UNIT=3390,
//   VOL=SER=ZZSMS3,SPACE=(CYL,(5,5))
