//FORSYSXZ JOB  CLASS=A,MSGCLASS=X,REGION=0M
//* ------------------------------------------------------------------
//*  ZZAC : INSTALLATION TWS 950
//* ----------------------------------------------------------
//* TWS950 : ALLOCATION DES FICHIERS UNIQUES DIAGNOSTIC/VIDAGES ZZnF
//* TWS950 : ALLOCATION DES FICHIERS UNIQUES DIAGNOSTIC/VIDAGES ZZnG
//* Passer ce job une fois pour ZZnF avec SYSTWS=F
//* Puis passer pour ZZnG avec SYSTWS=G
//* ----------------------------------------------------------
//E1       EXPORT SYMLIST=(SYSTWS)
//SYSTWS   SET    SYSTWS=F
//*SYSTWS   SET    SYSTWS=G
//*********************************************************************
//* NOTE: IN A PARALLEL SYSPLEX ENVIRONMENT, THE DATASETS ARE MVS  @X3A
//*       IMAGE UNIQUE AND AS SUCH, GOOD CANDIDATES FOR BEEING     @X3A
//*       REFERRED WITH MVS STATIC SYMBOLS IN THE   ZWS   STC JCL  @X3A
//*********************************************************************
//DELETE   EXEC PGM=IDCAMS,REGION=512K
//SYSPRINT DD SYSOUT=*
//SYSIN    DD     *,SYMBOLS=JCLONLY
  DELETE ZZOSSMS.TWTZ.&SYSTWS..SYSDUMP
  DELETE ZZOSSMS.TWTZ.&SYSTWS..MLOG
  DELETE ZZOSSMS.TWTZ.&SYSTWS..MLOG2
  DELETE ZZOSSMS.TWTZ.&SYSTWS..EQQDUMP
  SET MAXCC = 0
//*
//*********************************************************************
//* ALLOCATE THE SYSMDUMP FILE USED IN ZWS  ADDRESS SPACE
//* IEFBR14 IS USED TO ALLOCATE DATA SET
//*
//* ddname: SYSMDUMP                                             @02A
//*                                               DS size change @33C
//*********************************************************************
//ALLOCDMP EXEC PGM=IEFBR14
//*
//SYSPRINT DD SYSOUT=*
//SYSUT1   DD DSN=ZZOSSMS.TWTZ.&SYSTWS..SYSDUMP,
//         DCB=(RECFM=F,BLKSIZE=4160,LRECL=4160,DSORG=PS),
//         UNIT=3390,SPACE=(CYL,(20,20)),DISP=(NEW,CATLG)
//*
//*********************************************************************
//* ALLOCATE THE MLOG FILE USED BY THE  ZWS    SUBSYSTEM
//* IEBGENER IS USED TO ALLOCATE THE DATA SET
//*
//* ddname: EQQMLOG and EQQMLOG2 (used for switch mlog)          @81C
//*
//*********************************************************************
//ALLOCMLG EXEC PGM=IEBGENER
//*
//SYSPRINT DD SYSOUT=*
//SYSUT1   DD DUMMY,DCB=(RECFM=F,BLKSIZE=820,LRECL=820)
//SYSUT2   DD DSN=ZZOSSMS.TWTZ.&SYSTWS..MLOG,UNIT=3390,
//         DCB=(RECFM=VBA,BLKSIZE=1632,LRECL=125,DSORG=PS),
//         SPACE=(CYL,(1,1)),DISP=(NEW,CATLG)
//SYSIN    DD DUMMY
/*
//SYSPRINT DD SYSOUT=*
//SYSUT1   DD DUMMY,DCB=(RECFM=F,BLKSIZE=820,LRECL=820)
//SYSUT2   DD DSN=ZZOSSMS.TWTZ.&SYSTWS..MLOG2,UNIT=3390,
//         DCB=(RECFM=VBA,BLKSIZE=1632,LRECL=125,DSORG=PS),
//         SPACE=(CYL,(1,1)),DISP=(NEW,CATLG)
//SYSIN    DD DUMMY
/*
//*
//*********************************************************************
//* ALLOCATE THE   ZWS   DIAGNOSTIC DUMP DATA SET
//* IEFBR14  IS USED TO ALLOCATE THE PARTITIONED DATA SET
//*
//* ddname: EQQDUMP                                              @02A
//*
//*********************************************************************
//ALLOCDMP EXEC PGM=IEBGENER
//*
//SYSPRINT DD SYSOUT=*
//SYSUT1   DD *
/*
//SYSUT2   DD DSN=ZZOSSMS.TWTZ.&SYSTWS..EQQDUMP,UNIT=3390,
//         DCB=(RECFM=FB,BLKSIZE=3120,LRECL=80),
//         SPACE=(TRK,(5,5)),DISP=(NEW,CATLG)
//SYSIN    DD DUMMY
/*
