//FORSYSXZ JOB  CLASS=A,MSGCLASS=X,REGION=0M,NOTIFY=&SYSUID
//* ------------------------------------------------------------------
//*  ZZAC :  MIRE TCPIP ZZOS SPECIALE ZZAC pour ZZ5F
//*  ZZAC :  V TCPIP,TN3270,O,ZZOS.PLEXZZOS.TCPPARMS(TN3270)
//* ------------------------------------------------------------------
//*
//*  THIS JOB CHANGES THE SCREEN THAT VTAM DISPLAYS AT TERMINALS
//*  THAT HAVE NO ACTIVE SESSIONS (THOSE ON WHICH NOBODY IS LOGGED
//*  ON TO A VTAM APPLICATION).
//*
//ASSEMB   EXEC PGM=ASMA90,REGION=1024K,
//             PARM='NODECK,OBJECT'
//SYSPRINT DD  SYSOUT=*
//SYSLIB   DD  DSN=SYS1.MACLIB,DISP=SHR
//         DD  DSN=SYS1.SISTMAC1,DISP=SHR
//SYSUT1   DD  UNIT=SYSALLDA,SPACE=(CYL,(20,5))
//SYSUT2   DD  UNIT=SYSALLDA,SPACE=(CYL,(10,1))
//SYSUT3   DD  UNIT=SYSALLDA,SPACE=(CYL,(2,1))
//SYSUT4   DD  UNIT=SYSALLDA,SPACE=(CYL,(2,1))
//SYSUT5   DD  UNIT=SYSALLDA,SPACE=(CYL,(2,1)),DCB=SYS1.MACLIB,
//             DISP=(,PASS)
//SYSLIN   DD DSN=&&OBJ(USSTCP5F),DISP=(,PASS),
//            SPACE=(CYL,(1,1,1)),UNIT=SYSALLDA
//SYSIN    DD  *
********************************************************
***    USSTABLE DE LA NOUVELLE CONFIGURATION.        ***
********************************************************
USSLOCAL USSTAB FORMAT=DYNAMIC,DATEFRM=DMY
LOGON    USSCMD CMD=LOGON,FORMAT=BAL
         USSPARM PARM=APPLID,DEFAULT='TSO'
         USSPARM PARM=LOGMODE
         USSPARM PARM=P1,REP=DATA,DEFAULT=' '
*
TSO      USSCMD CMD=TSO,REP=LOGON,FORMAT=BAL
         USSPARM PARM=APPLID,DEFAULT='A06TSO'
         USSPARM PARM=LOGMODE
         USSPARM PARM=P1,REP=DATA
*
TSOF     USSCMD CMD=TSOF,REP=LOGON,FORMAT=BAL
         USSPARM PARM=APPLID,DEFAULT='A06TSO'
         USSPARM PARM=LOGMODE
         USSPARM PARM=P1,REP=DATA
*
TSOG     USSCMD CMD=TSOG,REP=LOGON,FORMAT=BAL
         USSPARM PARM=APPLID,DEFAULT='A07TSO'
         USSPARM PARM=LOGMODE
         USSPARM PARM=P1,REP=DATA
*
CICS     USSCMD CMD=CICS,REP=LOGON,FORMAT=BAL
         USSPARM PARM=APPLID,DEFAULT='CICSTS56'
         USSPARM PARM=LOGMODE
         USSPARM PARM=P1,REP=DATA
*
ZZAC     USSCMD CMD=ZZAC,REP=LOGON,FORMAT=BAL
         USSPARM PARM=APPLID,DEFAULT='CICSZZAC'
         USSPARM PARM=LOGMODE
         USSPARM PARM=P1,REP=DATA
*
IMS      USSCMD CMD=IMS,REP=LOGON,FORMAT=BAL
         USSPARM PARM=APPLID,DEFAULT='IMS15'
         USSPARM PARM=LOGMODE
         USSPARM PARM=P1,REP=DATA
*
MSG10    USSMSG MSG=10,BUFFER=(MSG10BFR,SCAN)
MSG10BFR DC AL2(MSGEND-MSG10BFR)  *** BUFFER LENGTH
*
STREAM   DS    0C
*        DC    X'27'       ESCAPE CHAR POUR TSO
         DC    X'F5'       ERASE/WRITE
         DC    X'C7'       WCC
LINE1    DC    X'1DF0'     PROTEGE
         DC    X'2903'     SFE
         DC    X'C0F0'     ATTR #1
         DC    X'4100'     ATTR #2
         DC    X'42F1'     ATTR #3
         DC    C'                      Nous sommes le @@@@DATE,'
         DC    C' @@@@TIME'
*
LINE3    DC    X'11',AL2(((03-1)*80)+(01-1))            SBA R=03,C=01
         DC    X'2903'     SFE 3 ATTRIBUTS A SUIVRE
         DC    X'C0F0'     ATTR #1 BASIC = PROTEGE
         DC    X'4100'     ATTR #2 DOUBLE BRILLANCE = Defaut
         DC    X'42F5'     ATTR #3 COULEUR TURQUOISE
         dc    c'   SSSSSSS IIII  IIII'
         DC    X'2903'     SFE 3 ATTRIBUTS A SUIVRE
         DC    X'C0F0'     ATTR #1 BASIC = PROTEGE
         DC    X'4100'     ATTR #2 DOUBLE BRILLANCE = Defaut
         DC    X'42F1'     ATTR #3 COULEUR BLEUE
         DC    C' ------ZZAC----ZZAC----ZZAC----Z'
         DC    C'ZAC----ZZAC----ZZAC---'
         DC    X'1DF0'     SF DELIMITEUR DE CHAMP
*
LINE4    DC    X'11',AL2(((04-1)*80)+(01-1))            SBA R=04,C=01
         DC    X'2903'     SFE
         DC    X'C0F0'     ATTR #1
         DC    X'4100'     ATTR #2
         DC    X'42F5'     ATTR #3 COULEUR TURQUOISE
         DC    C'   SS       II    II   L     EEEEE  A A A  RRRRR   NN'
         DC    C'   N  I  NN   N  GGGGGG'
         DC    X'1DF0'     SF
*
LINE5    DC    X'11',AL2(((05-1)*80)+(01-1))            SBA R=05,C=01
         DC    X'2903'     SFE
         DC    X'C0F0'     ATTR #1
         DC    X'4100'     ATTR #2
         DC    X'42F5'     ATTR #3
         DC    C'   SS       II    II   L     E      A   A  R    R'
         DC    C'  N N  N  I  N N  N  G'
         DC    X'1DF0'     SF
*
LINE6    DC    X'11',AL2(((06-1)*80)+(01-1))            SBA R=06,C=01
         DC    X'2903'     SFE
         DC    X'C0F0'     ATTR #1
         DC    X'4100'     ATTR #2
         DC    X'42F5'     ATTR #3
         DC    C'   SSSSSSS  II    II   L     EEEEE  AAAAA  RRRRRR'
         DC    C'  N  N N  I  N  N N  G  GGG'
         DC    X'1DF0'     SF
*
LINE7    DC    X'11',AL2(((07-1)*80)+(01-1))            SBA R=07,C=01
         DC    X'2903'     SFE
         DC    X'C0F0'     ATTR #1
         DC    X'4100'     ATTR #2
         DC    X'42F5'     ATTR #3
         DC    C'        SS  II    II   L     E      A   A  R    R'
         DC    C'  N   NN  I  N   NN  G    G'
         DC    X'1DF0'     SF
*
LINE8    DC    X'11',AL2(((08-1)*80)+(01-1))            SBA R=08,C=01
         DC    X'2903'     SFE
         DC    X'C0F0'     ATTR #1
         DC    X'4100'     ATTR #2
         DC    X'42F5'     ATTR #3
         DC    C'        SS  II    II   LLLLL EEEEE  A   A  R    R'
         DC    C'  N    N  I  N    N  GGGGGG'
         DC    X'1DF0'     SF
*
LINE9    DC    X'11',AL2(((09-1)*80)+(01-1))            SBA R=09,C=01
         DC    X'2903'     SFE
         DC    X'C0F0'     ATTR #1
         DC    X'4100'     ATTR #2
         DC    X'42F5'     ATTR #3
         DC    C'   SSSSSSS IIII  IIII'
         DC    X'2903'     SFE 3 ATTRIBUTS A SUIVRE
         DC    X'C0F0'     ATTR #1 BASIC = PROTEGE
         DC    X'4100'     ATTR #2 DOUBLE BRILLANCE = Defaut
         DC    X'42F1'     ATTR #3 COULEUR BLEUE
         DC    C' ------ZZAC----ZZAC----ZZAC----Z'
         DC    C'ZAC----ZZAC----ZZAC---'
         DC    X'1DF0'     SF
*
LINE13   DC    X'11',AL2(((13-1)*80)+(01-1))            SBA R=13,C=01
         DC    X'2903'     SFE
         DC    X'C0F0'     ATTR #1
         DC    X'4100'     ATTR #2
         DC    X'42F1'     ATTR #3
         DC    C'   �IP  '
         DC    X'2903'     SFE
         DC    X'C0F0'     ATTR #1
         DC    X'4100'     ATTR #2
         DC    X'42F3'     ATTR #3
         DC    C':'
         DC    X'2903'     SFE
         DC    X'C0F0'     ATTR #1
         DC    X'4100'     ATTR #2
         DC    X'42F1'     ATTR #3
         DC    C'@@@@@@@@@IPADDR'
         DC    X'2903'     SFE
         DC    X'C0F0'     ATTR #1
         DC    X'4100'     ATTR #2
         DC    X'42F1'     ATTR #3
         DC    C'Luname'
         DC    X'2903'     SFE
         DC    X'C0F0'     ATTR #1
         DC    X'4100'     ATTR #2
         DC    X'42F3'     ATTR #3
         DC    C':'
         DC    X'2903'     SFE
         DC    X'C0F0'     ATTR #1
         DC    X'4100'     ATTR #2
         DC    X'42F1'     ATTR #3
         DC    C'@@LUNAME'
         DC    X'2903'     SFE
         DC    X'C0F0'     ATTR #1
         DC    X'4100'     ATTR #2
         DC    X'42F1'     ATTR #3
         DC    C'   z/OS'
         DC    X'2903'     SFE
         DC    X'C0F0'     ATTR #1
         DC    X'4100'     ATTR #2
         DC    X'42F3'     ATTR #3
         DC    C':'
         DC    X'2903'     SFE
         DC    X'C0F0'     ATTR #1
         DC    X'4100'     ATTR #2
         DC    X'42F1'     ATTR #3
         DC    C'Version 2.4  (ZZ5F)'
         DC    X'1DF0'     SF
*
LINE15   DC    X'11',AL2(((15-1)*80)+(01-1))            SBA R=15,C=01
         DC    X'2903'     SFE
         DC    X'C0F0'     ATTR #1
         DC    X'4100'     ATTR #2
         DC    X'42F6'     ATTR #3
         DC    C'                              -------------'
         DC    X'1DF0'     SF
*
LINE17   DC    X'11',AL2(((17-1)*80)+(01-1))            SBA R=17,C=01
         DC    X'2903'     SFE
         DC    X'C0F0'     ATTR #1
         DC    X'4100'     ATTR #2
         DC    X'42F6'     ATTR #3
         DC    C'   APPLICATIONS DISPONIBLES :'
         DC    X'1DF0'     SF
*
* LINE18   DC    X'11D5D1'   SBA LINE 8/2
LINE18   DC    X'11',AL2(((18-1)*80)+(01-1))            SBA R=18,C=01
         DC    X'2903'     SFE
         DC    X'C0F0'     ATTR #1
         DC    X'4100'     ATTR #2
         DC    X'42F6'     ATTR #3
         DC    C'         - TSO    Tapez : TSO userid (def ZZ5F) ou '
         DC    C'TSOF/TSOG pour ZZ5F/ZZ5G'
         DC    X'1DF0'     SF
*
*LINE19   DC    X'11D661'   SBA
LINE19   DC    X'11',AL2(((19-1)*80)+(01-1))            SBA R=19,C=01
         DC    X'2903'     SFE
         DC    X'C0F0'     ATTR #1
         DC    X'4100'     ATTR #2
         DC    X'42F6'     ATTR #3
         DC    C'         - ZZAC   Tapez : ZZAC pour le'
         DC    C' CICS Ma French ZZAC'
         DC    X'1DF0'     SF
*
LINE20   DC    X'11',AL2(((20-1)*80)+(01-1))            SBA R=20,C=01
         DC    X'2903'     SFE
         DC    X'C0F0'     ATTR #1
         DC    X'4100'     ATTR #2
         DC    X'42F6'     ATTR #3
         DC    C'         - CICS   Tapez : CICS '
         DC    X'1DF0'     SF
*
LINE21   DC    X'11',AL2(((21-1)*80)+(01-1))            SBA R=21,C=01
         DC    X'2903'     SFE
         DC    X'C0F0'     ATTR #1
         DC    X'4100'     ATTR #2
         DC    X'42F6'     ATTR #3
         DC    C'         - Autres Tapez : LOGON APPLID=xxxx'
         DC    X'1DF0'     SF
*
LINE23   DC    X'11',AL2(((23-1)*80)+(01-1))            SBA R=23,C=01
         DC    X'2903'     SFE
         DC    X'C0F0'     ATTR #1
         DC    X'4100'     ATTR #2
         DC    X'42F4'     ATTR #3
         DC    C'   COMMANDE ===>'
*
CMD      DC    X'115BF2'   SBA
         DC    X'2903'     SFE
         DC    X'C0C0'     ATTR #1 BASIC = UNPROTECTED
         DC    X'4100'     ATTR #2 (HIGHLIGHTING = DEFAULT)
         DC    X'42F2'     ATTR #3 (COLOR = RED)
         DC    X'115BF313' SBA + INSERT CURSOR
MSGEND   EQU    *
         USSEND
         END
//LKED     EXEC PGM=HEWLH096,COND=(0,LT),
//         PARM=('SIZE=(1000K,100K),NCAL,XREF,LET,LIST')
//SYSPRINT DD SYSOUT=*
//SYSLMOD  DD DSN=ZZOS.PLEXZZOS.VTAMLIB,DISP=SHR
//SYSUT1   DD UNIT=SYSALLDA,SPACE=(6160,(230,760))
//OBJECT   DD DSN=*.ASSEMB.SYSLIN,VOL=REF=*.ASSEMB.SYSLIN,DISP=SHR
//*****************************************************
//SYSLIN   DD *
  INCLUDE OBJECT(USSTCP5F)
  NAME USSTCP5F(R)
/*
//
