//ZZACPCLN JOB  CLASS=A,MSGCLASS=X,REGION=0M,USER=SYSADM
//* ------------------------------------------------------------------
//*  ZZAC :  FREE PACKAGE COLLECTION ZZACCOLL
//* ------------------------------------------------------------------
//STGZZAC  EXEC PGM=IKJEFT01,DYNAMNBR=20
//STEPLIB  DD  DISP=SHR,DSN=DB2Z.SDSNEXIT
//SYSTSPRT DD SYSOUT=*
//SYSPRINT DD SYSOUT=*
//SYSUDUMP DD SYSOUT=*
//SYSTSIN  DD *
  DSN SYSTEM(DB2Z)
  FREE PACKAGE(ZZACCOLL.*)
  END
//*
