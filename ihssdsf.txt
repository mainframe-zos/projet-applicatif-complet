�\INMR01 �  &  JES2ZZM  �SYSJRG  N1  �SYSJRG�  20210214163441
  ^\INMR02   �  INMCOPY�  �  �0      �  �   � �  �  "8 �  & 
  ZZOSSMS IHS �SDSF�\INMR03�  �  �0      �   & �   {/* Rexx */{{
/* This code is provided as is by IBM{{   No warranty implied{�{   Use at own
 risk{{   Author: Edward McCarthy�{           Feb 2013�{*/�{/* This rexx is us
ed to display active and completed STC content  */{+{/* ***********************
*********************************************** */�{/* BEGIN MAIN CODE */+{/* **
******************************************************************** */{&{/* ma
inImageName - name of an image file to display at the top of each page on�{
              the browser|{                   Code expects image file to be loca
ted in the image sub-dir{                   where the IBM HTTP server is config
ured in.{                   The default value of ../images/zec12.jpg refers to t
he�{                   images sub directory in the htdocs directory�{*/{{mainI
mageName = "./images/zec12.jpg"{�{/* sdsfRexxPath - Value defined in ScriptAlia
s directive in the httpd.conf file<{                  The ScriptAlias directive
points to the actual directory�{                  containing this Rexx�{*/{�{sd
sfRexxPath = '/'mvsvar(SYSNAME)'/tools/sdsf'{*{/* These two parameters control
how this code handles output with large number of lines */{;{/* maxLines - this
 specifies the maximum number of lines that will be sent to the browser */;{/*
          As a guide, 5000 lines of output is approximately 400,000 bytes
      */;{/*            You can increase or decrease this parameter as you requi
re                  */;{/*            Large values will mean larger amount of da
ta sent to browser in one go,     */;{/*            which will take longer to lo
ad etc and use more CPU on zOS to send etc      */{;{/* maxLinesRead - this par
ameter is used to control how many lines are read from a STC    */;{/*
      at a time, if the code tried to read a very large number of lines in    */
;{/*                one go it would cause an out of memory condition
            */{;{/* value of maxLinesread must be less then value of maxLines
                            */{{maxLines = 5000�{maxLinesRead = 2000{�{/* all
owStcDownload - used to allow all the output of a STC to be downloaded as a�{
                   compressed zip file�{               Must be set to 1 to allow
 this capability�{                      If set, then an icon is displayed that t
he user can click on�{               to get STC output downloaded as a gz file�{
                      Set to 0 to disable�{*/{{allowStcDownload = 1{�{/* allo
wStcDDNameDownload - used to allow DD sections of a STC to be downloaded as a�{
                           compressed zip file {                     Must be set
 to 1 to allow this capability!{                            If set, then an icon
 is displayed that the user can click on&{                     to get DD section
 of a STC output downloaded as a gz file�{                            Set to 0 t
o disable�{*/{{allowStcDDNameDownload = 1{�{/* supportContact - set this to a
 string, which will be displayed as part of error�{                    messages
displayed if an error occurs�{*/{{supportContact = 'your z/OS system programme
r on 555-1234'{{/* gzLoc - Name of directory containing the gzip program */{
{gzLoc = "/local/tools/bin"{�{CONTENT_LENGTH=GETENV('CONTENT_LENGTH'){REQUESTB
ODY=CHARIN(,,CONTENT_LENGTH)
{QUERY_STRING=GETENV('QUERY_STRING')�{SERVER_PROTOC
OL=GETENV('SERVER_PROTOCOL')�{SERVER_SOFTWARE=GETENV('SERVER_SOFTWARE'){�{zOSHo
stNameAndPort = GETENV('HTTP_HOST'){�{/* Initialise variables used to keep trac
k of what we are displaying */{{numvars = 0{prefix=''{display=''{jobname=''
{ddname=''�{dsid=''{firstLine=''{lastLine=''{download=''�{lpar=''{�{/* Scan
 the received query string of the URL to find any keywords and their values */{
{IF query_string <> '' THEN�{DO�{  in = query_string{  DO I = 1 BY 1 UNTIL in=
''{    PARSE VAR in key.i'='val.i'&' In�{    if key.i='display' then display=va
l.i�{    if key.i='jobname' then jobname=val.i�{    if key.i='prefix' then prefi
x=val.i{    if key.i='jobid' then jobid=val.i�{    if key.i='ddname' then ddnam
e=val.i
{    if key.i='dsid' then dsid=val.i{    if key.i='firstLine' then firs
tLine=val.i{    if key.i='lastLine' then lastLine=val.i{    if key.i='download
' then download=val.i
{    if key.i='lpar' then lpar=val.i
{    /* Fail immediat
ely if asked */�{    if key.i='pleasefail' then do�{       say 'CGI script faili
ng'{       exit 1�{    end{    numvars = i{  END	{END{<{/* The following sec
tion of the code handles creation of a compressed file�{   that contains the sel
ected STC output and sending of this file to the user */{�{if download = "1" th
en do{{   rc=isfcalls("ON"){   rc2 = rc{   if rc2 != 0 then DumpRexxErrMsgs(
'sv001' rc2){�{   /* Do calls to SDSF to get the output for the STC to be downl
oaded */{{   isfprefix=jobname{   isfowner="*"{   isfsysname='*'{�{   Addre
ss SDSF "ISFEXEC "display{   rc2 = rc{   if rc2 != 0 then DumpRexxErrMsgs('sv0
02' rc2){�{   do ix=1 to JNAME.0�{      if JNAME.ix = jobname & JOBID.ix = jobi
d{      then jtoken = TOKEN.ix�{   end{{   opts_sdsf = "(PREFIX J)"{.{   /*
If no ddname value received then will get all output for the STC */{�{   if ddn
ame = "" then do�{      /* Get all the output for a selected STC */�{      gzNam
e = jobname'.'jobid'.txt.gz'|{      Address SDSF "ISFACT "display" TOKEN('"jtoke
n"') PARM(NP SA)" opts_sdsf{      rc2 = rc�{      if rc2 != 0 then DumpRexxErrM
sgs('sv003' rc2){({      /* When getting all the output for a STC, each part of
 the STC output<{         is in own ISFDDNAME, so here we create a string of all
 of them */{{      isfDDs = isfddname.1�{      do ii = 2 to isfddname.0�{
    isfDDs = isfDDs","isfddname.ii{      end{|{      /* The next line concaten
tates the list of DD names to the first one */{{      call bpxwdyn "concat ddL
ist("isfDDs")"{      rc2 = rc�{      if rc2 != 0 then DumpRexxErrMsgs('sv004' r
c2)�{   end{   else do�{       /* Get output for a selected part of the STC */�
{       gzName = jobname'.'jobid'.'ddname".txt.gz"|{       Address SDSF "ISFACT
"display" TOKEN('"jtoken"') PARM(NP ?)" opts_sdsf{       rc2 = rc�{       if rc
2 != 0 then DumpRexxErrMsgs('sv005' rc2){{       do ii = 1 to JDDNAME.0�{
     if JDDNAME.ii = ddname & JDSID.ii = dsid{          then do�{           ddt
oken = jtoken.ii�{           recCnt = JRECCNT.ii�{           dd = ii{
end{       end�{       opts_sdsf = "(PREFIX X)"�{       Address SDSF "ISFACT "d
isplay" TOKEN('"ddtoken"') PARM(NP SA)" opts_sdsf{       rc2 = rc�{       if rc
2 != 0 then DumpRexxErrMsgs('sv006' rc2){       isfDDs = isfddname.1�{   end{�
{   /* By here have one DD name pointing to the output for the STC */{�{   /* S
end HTTP Headers to tell browser sending a compressed file */{�{   say 'Content
-Type: application/gzip'{   say 'Content-Disposition: attachment; filename="'gz
Name'"'{   say ''{�{   /* The following lines reads STC output from the alloca
ted DD name.{      pipes this to the iconv function to convert it from EBCDIC to
 ASCII&{      then pipes this to the gzip function, the -f ( force ) option caus
es the�{      compressed out to go to stdout, which results in the compressed ou
tput just+{      being sent directly back to the browser, which means we do not
need to�{      create a gz file on disk */{�{   rv=bpxwunix("iconv -f IBM-1047
-t ISO8859-1 | "gzLoc"/gzip -9c -f ","DD:"isfDDs,,){{   exit 0{	{end{�{/* Th
e following section of the code handles displaying output on the browser */{{/
* Set content type of HTML we send to the browser */{{say 'Content-type: text/
html;charset=UTF-8'{�{/* Next line - very important */�{/* It separates the HTT
P header from the body of the reponse */�{/* Do no delete it */{�{say ''{{rc=
isfcalls("ON")�{rc2 = rc{if rc2 != 0 then DumpRexxErrMsgs('sv101' rc2){{isfpr
efix="*"{isfowner="*"{isfsysname='*'{if display <> 'st' then do�{   if lpar =
 'ALL' then isfsysname = '*'�{   else isfsysname = lpar	{end{�{say "<title>IBM
z/OS SDSF Viewer</title>"{say "<html>"{say '<body >'{{/* This is css used to
 make the display more presentable */{
{say '  <style type="text/css">    '�{sa
y '   tr.d0 {color: #000; background: #E9D7F8 } '�{say '   tr.d1 {color: #000; b
ackground: #F5F8B0 }'�{say '   .linesTable '{say '   { font-size:18pt; font-wei
ght:bold '{say '   }'�{say '   th.c1 {color: blue; background: #F5F8B0 }'�{say
'   th.c2 {color: blue;  }'�{say '   .zc1 {color: #CC6600;  }'�{say '   .zc2 {co
lor: #421C52;  }'�{say '   .zc3 {color: #732C7B;  }'�{say '   .zc4 {color: green
; font-size:16pt; font-weight:bold }'�{say '   option.o1 {color: green; font-siz
e:12pt; }'�{say '   option.o2 {color: #A5A162; font-size:12pt;}'{say '  </style
>    '{�{/* Javascript used to help format the STC output in the browser */{{
say '<script type="text/javascript" language="JavaScript">'�{say 'var usingIE= f
alse;'{{say 'function scanSdsfLines() {     '{say "sdsfLinesId = document.get
ElementById('sdsfLines'); "�{say "sdsfLines = sdsfLinesId.innerHTML; "�{say 'var
 sdsfLines=sdsfLinesId.innerHTML.split("\n");'{say "var updSdsfLines = '';"�{sa
y "var cntLines = sdsfLines.length-1;"{say 'for(var i=0;i<sdsfLines.length;i++)
 {'�{say '   if ( i > 0 && i < cntLines-1 ) { '{say '      if ( usingIE ) {'�{s
ay '      updSdsfLines += sdsfLines[i].replace(/ /g, "&nbsp;").replace(/</g, "&l
t;") + "<br>";'�{say '      } else { '�{say '              updSdsfLines += sdsfL
ines[i].replace(/</g, "&lt;") + "<br>";'{say '      }'{say '   }'�{say '}'�{sa
y "document.getElementById('sdsfLines').innerHTML = updSdsfLines;"�{say '}'{�{s
ay 'function viewSelectedDDname()' ||,{    '{'||,h{    'var URL = document.ddna
meForm.url_list.options[document.ddnameForm.url_list.selectedIndex].value; windo
w.location.href = URL;'||,�{    '}'{�{say 'function viewSelectedLinesForStcDDna
me()' ||,{    '{'||,�{    'var URL = document.stcLinesForm.url_list.options[doc
ument.stcLinesForm.url_list.selectedIndex].value; window.location.href = URL;'||
,�{    '}'{say '</script>'{say '<!--[if IE]>' ||,�{    '<script type="text/jav
ascript">' ||,�{    '       usingIE = true;' ||,�{    '</script>'||,�{    '<![en
dif]-->'{�{/* Display main title */{{say '<h1>'�{say '<IMG SRC="'mainImageNam
e'" HEIGHT="50" WIDTH="50" BORDER="0">'�{say '<font color="blue">IBM z/OS SDSF V
iewer</font>     '{say "</h1>"{�{if display = "syslog" then showZosSyslog(){
{if display = "whoami" then DumpRexxErrMsgs('sv300' 0){�{if display = "da" then
 do{   displayCmd = 'DA'{   displayTitle = 'Active'�{   displayparm = 'da'	{en
d�{else do{   displayCmd = 'ST'{   displayTitle = 'Completed'�{   displayParm
= 'st'	{end{{/* If on main menu or display of STCs then */{/* Display form to
 allow prefix to be set */{{if display = "" | ( display <> "" & jobname = "" )
 then do�{   say '<form action="sdsfViewer.rx" method="get"> '
{   say '<table C
LASS="linesTable">'�{   say '<td valign="top"><span class="zc1">Prefix: </span><
/td><td valign="top"><span class="zc2">'||,�{     '<input type="text" name="pref
ix" value="'prefix'"></td>'||,�{     '<td valign="top"><span class="zc2">'{{Ad
dress SDSF "ISFEXEC MAS"�{rc2 = rc{   lpar.0 = isfrows + 1{   lpar.1 = "ALL"{
   jj = 2{   do ii = 1 to isfrows{      lpar.jj = name.ii{      jj = jj + 1�{
   end{{   isfcols = ''{{   if display = 'da' | display = '' then do�{
say '<td valign="top"><span class="zc1">LPAR: </span></td><td valign="top"><span
 class="zc2">'{      say '<select class="zc4" name="lpar" >'�{      do ii = 1 t
o lpar.0�{         say '<option value="'||,�{         lpar.ii||'"'{         if
lpar.ii = lpar then say ' selected class="o1"'�{         else say ' class="o2"'�
{         say '>'||,{            lpar.ii||,{            '</option>'{      end
{      say '</td>'�{   end�{   else say '<input type="hidden" name="lpar" value
="'lpar'">  '{�{   if display <> "" then({         say '<input type="hidden" na
me="display" value="'displayParm'">  '�{   say '<td><input type="submit" value="
Set Prefix and LPAR Scope">'||,{     '</span></td>'||,{     '</form>'{   say
'</td></table>'	{end{
{/* Display main menu if no parms */{{if display = "" t
hen do�{   say '<h2>View z/OS <a href="'sdsfRexxPath'/sdsfViewer.rx'||,�{
'?display=syslog&prefix='prefix'&lpar='lpar'">'||,�{       "Syslog</a></h2>"{
 say '<h2><a href="'sdsfRexxPath'/sdsfViewer.rx'||,{       '?display=da&prefix=
'prefix'&lpar='lpar'">'||,�{       "Active STC's</a></h2>"{   say '<h2><a href=
"'sdsfRexxPath'/sdsfViewer.rx'||,{       '?display=st&prefix='prefix'&lpar='lpa
r'">'||,�{       "Completed STC's</a></h2>"{   say "</html>"{   say "</body>"
{   exit 0	{end{�{/* Set SDSF prefix */{{isfprefix=prefix{�{/* isfsort used
to sort STC by jobname */{�{isfsort = "JNAME A"�{Address SDSF "ISFEXEC "display
Cmd�{rc2 = rc{if rc2 != 0 then DumpRexxErrMsgs('sv102' rc2){�{/* If end user h
as not selected a STC, then display list of STCs that match prefix */{{if jobn
ame = "" then do�{   say '<br><table CLASS="linesTable"><th><td valign="top"><a
href="'sdsfRexxPath'/sdsfViewer.rx?'||,{      'prefix='prefix'&lpar='lpar||,&{
     '"><img src="./icons/back.gif" title="Back to previous menu"/></a></td>'!{
  say '<td valign="top"><span class="zc3">'displayTitle" STC's for specified pre
fix"||,�{       '</span></td>'||,{     '</table>'{�{   if JNAME.0 = 0 then do�
{      say "<h2>There are no STC's for specified prefix</h2>"{      exit 0�{
end{{   zz = 0{�{      do xx=1 while isfcols <> ''�{           parse var isfc
ols parm isfcols�{           if parm <> 'TOKEN' & parm <> 'JOBNAME' then do{
        zz = zz + 1{           colId.zz = parm'.'{           colId.0 = zz{
        end{       end{       zz = 0{       do xx=1 while isftitles <> ''�{
         parse var isftitles "'"parm"'" isftitles{           if parm <> 'TOKEN'
 then do{           zz = zz + 1{           colTitle.zz = parm{           colT
itle.0 = zz{           end{       end{�{   say '<br><table class=ex1><tr>'�{
  if allowStcDownload = 1 then say '<td></td>'�{   say '<td>JobName</td>'�{
   do cc = 2 to colTitle.0�{           say '<td>'colTitle.cc'</td>'{        end
{   say '</tr>'{   stcCnt = 0{�{   do ix=1 to JNAME.0&{      if display = 'da
' | ( display = 'st' & QUEUE.ix <> "EXECUTION" ) then do{         stcCnt = stcC
nt + 1{�{         say '<tr class="d'stcCnt//2'">'�{         if allowStcDownload
 = 1 then do|{             say '<td valign="top"><a href="'sdsfRexxPath'/sdsfVie
wer.rx?'||,�{                 'display='display'&jobname='JNAME.ix||,{
        '&prefix='prefix||,{                 '&jobid='JOBID.ix||,�{
     '&download=1"'||,�{                 '&lpar='lpar||,�{                 ' tar
get="_blank"'||,;{                 '><img src="./icons/compressed.gif" title="Do
wnload as gz file"/></a></td>'{         end�{         say '<td><a href="'sdsfRe
xxPath'/sdsfViewer.rx?'||,{         'display='display'&jobname='JNAME.ix||,�{
                '&jobid='JOBID.ix||,{                  '&prefix='prefix||,�{
               '&lpar='lpar||,�{                  '">'JNAME.ix"</a></td>"{{
      do cc = 2 to colId.0�{            tt = colId.cc
{            Interpret "nn
="||tt||ix�{            say '<td>'nn'</td>'{         end�{        say '</tr>'{
      end�{   end�{   if stcCnt = 0 then do�{      say "<tr><th>No "displayTitle
" STC's for specified mask</th></tr>"{      exit 0�{   end{   say '</table>'{
   say "</html>"{   say "</body>"{�{   exit	{end{�{/* Find token for selected
 STC */{{jtoken = ''{isfprefix=jobname�{do ix=1 to JNAME.0{   if JNAME.ix =
jobname & JOBID.ix = jobid{      then jtoken = TOKEN.ix	{end{�{opts_sdsf = "(P
REFIX J)".{Address SDSF "ISFACT "displayCmd" TOKEN('"jtoken"') PARM(NP ?)" opts_
sdsf�{rc2 = rc{if rc2 != 0 then DumpRexxErrMsgs('sv102' rc2){�{/* Display list
 of DDNames for a selected STC when no DDName currently selected */{{if ddname
 = "" then do{�{   say '<table CLASS="linesTable"><th><td valign="top"><a href=
"'sdsfRexxPath'/sdsfViewer.rx?'||,{      'display='display||,�{      '&prefix='
prefix||,�{      '&lpar='lpar||,&{      '"><img src="./icons/back.gif" title="Ba
ck to previous menu"/></a></td>'�{   say '<td valign="top"><span class="zc1">Vie
wing: </span></td><td valign="top"><span class="zc2">'||,�{     jobname'</span><
/td><td valign="top"><span class="zc3"> '||,{     jobid '</span></td>'||,{
 '</table>'{{{   rc1 = rc�{   if rc1 > 0 then do�{      say "<h3>No output av
ailable</h3>"�{   end{   else do{       zz = 0
{       do xx=1 while isfcols2
<> ''{           parse var isfcols2 parm isfcols2�{           if parm <> 'TOKEN
' & parm <> 'DDNAME' then do{           zz = zz + 1{           colId.zz = parm
'.'{           colId.0 = zz{           end{       end{       zz = 0{
do xx=1 while isftitles2 <> ''{           parse var isftitles2 "'"parm"'" isfti
tles2{           if parm <> 'TOKEN' then do{           zz = zz + 1{
 colTitle.zz = parm{           colTitle.0 = zz{           end{       end�{
    say '<table ><tr>'�{       if allowStcDDNameDownload = 1 then say '<td></td>
'{�{        do cc = 1 to colTitle.0�{           say '<td>'colTitle.cc'</td>'{
       end�{        say '</tr>'�{        do ii = 1 to JDDNAME.0{         say '<
tr class="d'ii//2'">'�{         if allowStcDDNameDownload = 1 then do|{
    say '<td valign="top"><a href="'sdsfRexxPath'/sdsfViewer.rx?'||,�{
       'display='display'&jobname='jobname||,{                 '&prefix='prefix
||,�{                 '&jobid='jobid||,�{                 '&ddname='JDDNAME.ii'&
dsid='JDSID.ii||,�{                 '&lpar='lpar||,�{                 '&download
=1"'||,�{                 ' target="_blank"'||,;{                 '><img src="./
icons/compressed.gif" title="Download as gz file"/></a></td>'{         end{
      say '<td>'||,�{         '<a href="'sdsfRexxPath'/sdsfViewer.rx?'||,�{
    'display='display'&jobname='jobname||,{         '&jobid='jobid||,{
 '&prefix='prefix||,�{         '&lpar='lpar||,�{         '&ddname='JDDNAME.ii'&d
sid='JDSID.ii'">'JDDNAME.ii"</a></td>"{        do cc = 1 to colId.0{
tt = 'J'colId.cc�{          Interpret "nn="||tt||ii{          say '<td>'nn'</td
>'{        end�{        say '</tr>'�{     end�{     say '</table>'	{end{   say
 "</html>"{   say "</body>"{�{   exit	{end{{do ii = 1 to JDDNAME.0�{      if
 JDDNAME.ii = ddname & JDSID.ii = dsid{      then do�{           ddtoken = jtok
en.ii�{           recCnt = JRECCNT.ii�{           dd = ii{      end	{end{�{/*
If output is too large to display in one go */{/* then set parameter so that li
nk in URL has DSID value */{{dsidHtml = ""{if firstLine <> "" then�{   dsidHt
ml = '&ddname='ddname'&dsid='dsid{�{/* Display heading showing what part of STC
 is being display */{�{   say '<table CLASS="linesTable"><th><td valign="top"><
a href="'sdsfRexxPath'/sdsfViewer.rx?'||,{      'display='display'&jobname='job
name||,{         '&jobid='jobid||,{         '&prefix='prefix||,�{         '&lp
ar='lpar||,{         dsidHtml||,|{     '"><img src="./icons/back.gif" title="Ba
ck to previous menu"/></a></td>'�{   say '<td valign="top"><span class="zc1">Vie
wing: </span></td><td valign="top"><span class="zc2">'||,�{     jobname'</span><
/td><td valign="top"><span class="zc3"> '||,{     jobid '</span></td><td>'{{!
{   say '<form name="ddnameForm" method="get" action="'sdsfRexxPath'/sdsfsViewer
.rx">'||,�{    '<noscript>'||,&{    '<!-- use noscript to only show this if no j
avascript is available -->'||, {    '<input type="submit" name="submit_button" v
alue="Go">'||,{    '</noscript>'||,�{    '<select class="zc4" name="url_list" s
ize="1" onChange="viewSelectedDDname()">'{    do ii = 1 to JDDNAME.0�{        s
ay '<option value="'||,�{         sdsfRexxPath'/sdsfViewer.rx?'||,�{         'di
splay='display'&jobname='jobname||,{         '&jobid='jobid||,{         '&pref
ix='prefix||,�{         '&lpar='lpar||,{         '&ddname='JDDNAME.ii'&dsid='JD
SID.ii'"'�{        if ii = dd then say ' selected class="o1"'�{        else say
' class="o2"'�{        say '>'||,�{            jddname.ii||,{            '</opt
ion>'�{    end{    say '</select></form></td></th></table>'{�{/* If no output
for the selected DDName then advise end user */{{�{   if recCnt = 0 then do{
        say "<h3>No output to display</h3>"{         rc=isfcalls("OFF"){
   say "</html>"{         say "</body>"{      exit 0�{   end{{/* Get sysout
for selected DDName of STC */{�{opts_sdsf = "(PREFIX X)"({Address SDSF "ISFACT
"displayCmd" TOKEN('"ddtoken"') PARM(NP SA)" opts_sdsf�{rc2 = rc{if rc2 != 0 th
en DumpRexxErrMsgs('sv103' rc2){�{/* If output can be displayed in on go */�{/*
 OR                                */&{/* have already selected a range of lines
 of the selected DDName to display */�{/* Then display that output */{{if recC
nt < maxLines | firstLine > 0 then do{�{   /* If firstline not set, then will b
e displaying all output */{{   if firstLine = "" then do�{      firstLine = 1�
{      lastLine = spool.0�{   end{   else do<{          /* Display heading show
ing what lines are being displayed and */<{          /* provide links to go to p
revious or next set of lines       */{{          if recCnt < lastLine then las
tLine = recCnt{�{          say '<table CLASS="linesTable"><tr><td></td><td></td
><th class="c1">* Displaying *</th><td></td></tr>'{          say '<tr><th class
="c2" valign="top">Lines</td>'�{          if firstLine < maxLines then{
     say '<td></td>'�{          else say '<td valign="top">'||,{
   '<a href="'sdsfRexxPath'/sdsfViewer.rx?'||,�{                  'display='disp
lay'&jobname='jobname||,
{                  '&jobid='jobid||,{
 '&prefix='prefix||,�{                  '&lpar='lpar||,�{                  '&fir
stLine='firstLine-maxLines||,{                  '&lastLine='firstLine-1||,{
               '&ddname='ddname'&dsid='dsid'">'||,{                   firstLine
-maxLines'</a></td>'{!{         /* This code creates a drop down box, which dis
plays the current range of lines){            being displayed, and the entries i
n the drop down are all the other line ranges�{            allowing the user to
select one and automatically junp to that range */{�{         lb = recCnt % max
Lines�{         if lb * maxLines < recCnt then lb = lb + 1�{         say '<td><f
orm name="stcLinesForm" method="get" action="'sdsfRexxPath'/sdsfsViewer.rx">'||,
{             '<noscript>'||,�{             '<!-- use noscript to only show thi
s if no javascript is available -->'||,�{             '<input type="submit" name
="submit_button" value="Go">'||,{             '</noscript>'||,�{             '<
select class="zc4" name="url_list" size="1" onChange="viewSelectedLinesForStcDDn
ame()">'�{         do ii = 1 to lb�{            s1 = ( ii - 1) * maxLines + 1�{
           s2 = ii * maxlines{            say '<option value="'||,{
     sdsfRexxPath'/sdsfViewer.rx?'||,�{                'display='display'&jobnam
e='jobname||,�{                '&jobid='jobid||,�{                '&lpar='lpar||
,
{                '&prefix='prefix||,�{                '&firstLine='s1||,�{
            '&lastLine='s2||,�{                '&ddname='ddname'&dsid='dsid'"'${
                if (( ii - 1) * maxLines + 1) = firstLine then say ' selected cl
ass="o1"'�{                else say ' class="o2"'{                if ii = lb th
en s2 = recCnt{                say '>'||,�{                    s1'-'s2||,�{
                '</option>'{         end{         say '</select></form></td>'
{
{          if lastLine < recCnt then�{             say '<td valign="top">'||,
{                  '<a href="'sdsfRexxPath'/sdsfViewer.rx?'||,�{
  'display='display'&jobname='jobname||,
{                  '&jobid='jobid||,{
                 '&prefix='prefix||,�{                  '&lpar='lpar||,�{
           '&firstLine='firstLine+maxLines||,�{                  '&lastLine='las
tLine+maxLines||,{                  '&ddname='ddname'&dsid='dsid'">'||,�{
            firstLine+maxLines'</a></td>'{         else say '<td></td>'{{{
       say '</tr></table>'�{   end{{   /* This is where we display the output
*/{�{   say '<div><pre id="sdsfLines">'{   if recCnt < maxLines then{      li
nes = recCnt�{   else lines = maxLines{   readBlocks = lines % maxLinesRead{
 rr = lines // maxLinesRead{   if rr > 0 then readBlocks = readBlocks + 1�{   s
tartLine = firstLine{�{   /* We add this comment line to wrap all the output to
 be sent */�{   /* Once the browser gets all this content, it will invoke a java
script function */�{   /* which will remove the comments */&{   /* This is done
to prevent browser pre-processing the output it receives */!{   /* Need to do th
is as during testing found if output has certain strings like this */�{   /* ***
BUFFER OVERFLOW*** */�{   /* The browser seems to interpret this type of string
and get corrupted output */{{   say '<!--'{�{   /* If try to read very large
output files in one go, can get Out of Memory */�{   /* type errors, so we loop
and read in number of lines specified by maxLinesread */{{   do xx = 1 to read
Blocks*{      Address MVS "EXECIO "maxLinesRead" DISKR" isfddname.1 startLine "(
OPEN STEM spool.)"{      rc2 = rc�{      if rc2 != 0 then DumpRexxErrMsgs('sv10
4' rc2){      do ii = 1 to spool.0�{         say spool.ii{      end�{      sta
rtLine = startLine + maxLinesRead�{   end{�{   /* Add closing HTML comment */{
{   say '-->'{�{   /* Close the file have been reading */{�{   Address MVS "E
XECIO 0 DISKR" isfddname.1 "(FINIS"{   rc2 = rc{   if rc2 != 0 then DumpRexxEr
rMsgs('sv105' rc2)�{   say '</pre></div>'�{   say '<script type="text/javascript
"> scanSdsfLines()</script>'{{�{   rc=isfcalls("OFF")�{      say "</html>"�{
    say "</body>"{{   exit 0	{end�{else do{�{       /* Output selected has mo
re lines then maxLines limit */�{       /* We display a page with links to secti
ons of the output */{�{       outSections = recCnt % maxLines�{       rr = recC
nt // maxLines�{       if rr > 0 then outSections = outSections + 1{       firs
tLine = 1{       lastLine = maxLines{�{       say '<table ><tr><td><h3>Lines</
h3></td></tr><tr>'{�{       /* For each link we create, we read first line at t
hat line location and */�{       /* display it, may help end user decide which s
ection to display         */{�{       do ii = 1 to outSections�{         Addres
s MVS "EXECIO 1 DISKR" isfddname.1 firstLine "(OPEN STEM spool.)"{         rc2
= rc�{         if rc2 != 0 then DumpRexxErrMsgs('sv106' rc2){         say '<tr
class="d'ii//2'"><td>'||,�{         '<a href="'sdsfRexxPath'/sdsfViewer.rx?'||,�
{         'display='display'&jobname='jobname||,{         '&jobid='jobid||,{
       '&prefix='prefix||,�{         '&lpar='lpar||,�{         '&firstLine='firs
tLine||,�{         '&lastLine='lastLine||,{         '&ddname='ddname'&dsid='dsi
d'">'||,{         firstLine ||,{         "</a></td><td>"spool.1"</td></tr>"�{
        firstLine = firstLine + maxLines�{         lastLine = lastLine + maxLine
s{       end�{       Address MVS "EXECIO 0 DISKR" isfddname.1 "(FINIS"�{
say '</pre></div>'{�{       rc=isfcalls("OFF"){       say "</html>"{       sa
y "</body>"{       exit 0	{end{{end /* Display DA */{{{GETENV: PROCEDURE�{
  PARSE ARG envname{  RETURN ENVIRONMENT(envname){�{/*   The DumpRexxErrMsgs r
outine used to display error messages */{{DumpRexxErrMsgs:{{   Parse arg cod
eLoc retCode-{   Say '<p>Well this is not so good, contact 'supportContact' and
send them this info<br><br>'�{   Say 'Code location: 'codeLoc'<br><br>'�{   Say
'Return code: 'retCode '<br><br>'{   say 'isfmsg: 'isfmsg'<br>'�{   if isfmsg2.
0 > 0 then{      do ix=1 to isfmsg2.0{         Say isfmsg2.ix'<br>'{      end
{   say '<br>'{   Address SDSF "ISFEXEC WHO"�{   Say 'Output from ISFEXEC WHO<
br><br>'{{   do ix=1 to isfresp.0{       Say isfresp.ix'<br>'�{     end�{exit
{<{/*   The showZosSyslog routine used to display z/OS syslog to a browser */{
{showZosSyslog:{�{/*�{   Two variables control how much syslog is returned�{*/
{�{/* timeSpan - specifies how many minutes before the current time�{
    the start time for retrieving syslog info should be{              set to�{*
/{timeSpan = 10{�{/* maxLines - specifies the maximum number of lines that sho
uld�{              be retrieved from the syslog�{*/{maxLines = 2000{�{/* Get c
urrent date and time */{sDate = date("B"){eDate = date("B"){sTime = time("N")
{eTime = time("N")�{parse value sTime with hh ":" mm ":" ss{{/* following cal
ulates currtime minus value in timeSpan to{   determine start time�{*/{{timeS
panHH = timeSpan % 60�{pmm = mm - timeSpan{{if ( pmm < 0 ) then do{   pmm = m
m - timeSpan + 60{   hh = hh - timeSpanHH - 1�{   if ( hh < 0 )  then do{
 hh = 23{      sDate = sDate - 1�{   end	{end{if ( pmm < 10 ) then{     sTime
 = hh":0"pmm":"00�{else{     sTime = hh":"pmm":"00{sDate = Date('U',sDate,'B')
{eDate = Date('U',eDate,'B'){�{say '<br><table CLASS="linesTable"><th><td vali
gn="top"><a href="'sdsfRexxPath'/sdsfViewer.rx?'||,{      'prefix='prefix'&lpar
='lpar||,&{      '"><img src="./icons/back.gif" title="Back to previous menu"/><
/a></td>'{say '<td valign="top"><span class="zc3">'" z/OS Syslog"||,�{       '<
/span></td>'||,{     '</table>'{�{/* Display start and end time */{�{say ' '�
{say '<h3>Start date: ' sDate ' Time: ' sTime'</h2>'�{say '<h3>End date: ' eDate
 ' Time: ' eTime'</h3>'�{say ' '{{/* Set SDSF parms */{�{isflogstarttime=sTim
e�{isflogstartdate=sDate{�{isflinelim=maxLines�{say ' '{{/* Read syslog */{�
{Address SDSF "ISFLOG READ TYPE(SYSLOG)"�{rc2 = rc{if rc2 != 0 then DumpRexxErr
Msgs('sv201' rc2){{/* output the syslog */{�{if isfline.0 = 0 then do{   say
 'No syslog found  '{   exit 0	{end{�{say '<div><pre id="sdsfLines">'{do ix=1
 to isfline.0{   Say isfline.ix	{end�{say '</pre></div>'{
{if ( maxLines = isf
line.0 ) then do{   say ' '�{   say '<h4> Max lines limit on syslog display rea
ched'�{   say ' There may have been additional syslog output that was not retrie
ved due to max line limit</h4>'	{end{�{/* Issue command to get any WTO Reply me
ssages and display them */{{say '<h3>Outstanding WTO Reply messages</h3>'�{say
 '<div><pre id="sdsfLines2">'{�{Address SDSF "isfexec '/d r,l'"�{rc2 = rc{if r
c2 != 0 then DumpRexxErrMsgs('sv202' rc2){�{if isfulog.0 = 0 then do{   say 'N
o outstanding WTO Replies found  '{   exit 0	{end{{do ix=1 to isfulog.0{   S
ay isfulog.ix	{end�{say '</pre></div>'{�{exit{�\INMR06
