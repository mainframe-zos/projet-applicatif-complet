//FORSYSXZ JOB  CLASS=A,MSGCLASS=X,REGION=0M
//* ------------------------------------------------------------------
//*  ZZAC : CICS - Recopie en proclib
//* ------------------------------------------------------------------
//DFHPROC  EXEC PGM=IEBCOPY
//SYSPRINT DD SYSOUT=*
//DFHIN    DD DISP=SHR,DSN=SYS1.ZZOS.FORM.ZZAC
//DFHOUT   DD DISP=SHR,DSN=ZZOS.PLEXZZOS.PROCLIB
//SYSIN    DD *
  C I=((DFHIN,R)),O=DFHOUT
  S M=((DFHCZZAC,CICSZZAC,R))
//*
