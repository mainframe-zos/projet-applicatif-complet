  LOAD DATA LOG NO RESUME YES INDDN SYSREC00 INTO TABLE
      ZZACDB2Z.EMP
   (
   EMPNO                                  POSITION(       1         )
   CHAR(                      6)                ,
   FIRSTNME                               POSITION(       7         )
   VARCHAR                                      ,
   MIDINIT                                POSITION(      21         )
   CHAR(                      1)                ,
   LASTNAME                               POSITION(      22         )
   VARCHAR                                      ,
   WORKDEPT                               POSITION(      39         )
   CHAR(                      3)
        NULLIF(      42)='?',
   PHONENO                                POSITION(      43         )
   CHAR(                      4)
        NULLIF(      47)='?',
   HIREDATE                               POSITION(      48         )
   DATE EXTERNAL(            10)
        NULLIF(      58)='?',
   JOB                                    POSITION(      59         )
   CHAR(                      8)
        NULLIF(      67)='?',
   EDLEVEL                                POSITION(      68         )
   SMALLINT
        NULLIF(      70)='?',
   SEX                                    POSITION(      71         )
   CHAR(                      1)
        NULLIF(      72)='?',
   BIRTHDATE                              POSITION(      73         )
   DATE EXTERNAL(            10)
        NULLIF(      83)='?',
   SALARY                                 POSITION(      84:      88)
   DECIMAL
        NULLIF(      89)='?',
   BONUS                                  POSITION(      90:      94)
   DECIMAL
        NULLIF(      95)='?',
   COMM                                   POSITION(      96:     100)
   DECIMAL
        NULLIF(     101)='?'
   )
